#define USE_LittleFS

#include <FS.h>
#ifdef USE_LittleFS
  #define SPIFFS LITTLEFS
  #define LittleFS LITTLEFS
  #include <LITTLEFS.h>
#else
  #include <SPIFFS.h>
#endif
#include <ArduinoJson.h> // https://github.com/bblanchon/ArduinoJson
#include <WiFi.h>

//needed for library
#include <DNSServer.h>
#include <WebServer.h>
#include <WebSocketsClient.h>
#include <WiFiManager.h> //https://github.com/tzapu/WiFiManager

#include <NeoPixelBus.h>

#define colorSaturation 128
#define LED_PIN 13
#define RGB_PIN 2

RgbColor red(colorSaturation, 0, 0);
RgbColor green(0, colorSaturation, 0);
RgbColor blue(0, 0, colorSaturation);
RgbColor white(colorSaturation);
RgbColor black(0);

const uint16_t PixelCount = 24;
const uint8_t PixelPin = 4;

bool shouldSaveConfig = false;
long previousMillis = 0;
long interval = 1000 * 60 * 5;
WebSocketsClient webSocket;

NeoPixelBus<NeoGrbwFeature, NeoEsp32Rmt0800KbpsMethod> strip(PixelCount, PixelPin);
NeoPixelBus<NeoGrbFeature, NeoEsp32Rmt1800KbpsMethod> status(1, RGB_PIN);

void saveConfigCallback() {
  Serial.println("Should save config");
  shouldSaveConfig = true;
}

void updatePixels(JsonArray array) {
  // array with nulls or objects
  // objects with r,g,b,w
  size_t i = 0;
  for(JsonObject o : array) {
    if(o) {
      uint8_t r = o["r"].as<int>();
      uint8_t g = o["g"].as<int>();
      uint8_t b = o["b"].as<int>();
      uint8_t w = o["w"].as<int>();

      const RgbwColor c = RgbwColor(r, g, b, w);
      strip.SetPixelColor(i, c);
    }

    if (i++ > PixelCount) {
      break;
    }
  }

  strip.Show();
}

void webSocketEvent(WStype_t type, uint8_t *payload, size_t length) {
  switch (type) {
  case WStype_DISCONNECTED:
    Serial.printf("[WSc] Disconnected!\n");
    status.SetPixelColor(0, blue);
    status.Show();
    break;
  case WStype_CONNECTED: {
    Serial.printf("[WSc] Connected to url: %s\n", payload);
    status.SetPixelColor(0, green);
    status.Show();

    StaticJsonDocument<128> doc;
    doc["action"] = "subscribe";
    doc["key"] = "a1c60f30afb6b54fb04cca1ce2ea06dd6902a877c6acc97ec5176fa167928987";
    String json;
    serializeJson(doc, json);
    webSocket.sendTXT(json);
  } break;
  case WStype_TEXT: {
    if (payload == NULL || length == 0) {
      return;
    }
    Serial.printf("[WSc] get text: %s\n", payload);
    //StaticJsonDocument<1536> doc;
    DynamicJsonDocument doc(2048);
    DeserializationError error = deserializeJson(doc, payload, length);
    if (error) {
      Serial.print(F("deserializeJson() failed: "));
      Serial.println(error.f_str());
      return;
    }
    Serial.printf("[WSc] get json:\n");
    serializeJsonPretty(doc, Serial);
    Serial.println("");
    if (doc["update"]) {
      JsonArray array = doc["update"].as<JsonArray>();
      updatePixels(array);
    }
    break;
  }
  case WStype_BIN:
    Serial.printf("[WSc] get binary length: %u\n", length);
    //hexdump(payload, length);
    break;
  case WStype_ERROR:
  case WStype_FRAGMENT_TEXT_START:
  case WStype_FRAGMENT_BIN_START:
  case WStype_FRAGMENT:
  case WStype_FRAGMENT_FIN:
  case WStype_PING:
  case WStype_PONG:
    Serial.printf("[WSc] other event\n");
    break;
  }
}

void setupLittleFS() {
  //read configuration from FS json
  Serial.println("mounting FS...");

  // Initialize LittleFS
  if (LittleFS.begin()) {
    Serial.println("mounted file system");
    if (LittleFS.exists("/config.json")) {
      //file exists, reading and loading
      Serial.println("reading config file");
      File configFile = LittleFS.open("/config.json", "r");
      if (configFile) {
        Serial.println("opened config file");
        size_t size = configFile.size();
        // Allocate a buffer to store contents of the file.
        std::unique_ptr<char[]> buf(new char[size]);

        configFile.readBytes(buf.get(), size);
        DynamicJsonDocument jsonBuffer(1024);
        deserializeJson(jsonBuffer, buf.get());
        if (jsonBuffer.isNull()) {
          Serial.println("failed to load json config");
        } else {
          serializeJson(jsonBuffer, Serial);
          Serial.println("\nparsed json");
        }
      }
    }
  }
}

void setup() {
  // Initilize hardware:
  Serial.begin(115200);
  pinMode(LED_PIN, OUTPUT);

  // this resets all the neopixels to an off state
  strip.Begin();
  strip.Show();

  status.Begin();
  status.SetPixelColor(0, red);
  status.Show();

  setupLittleFS();

  WiFiManager wifiManager;
  //wifiManager.setBreakAfterConfig(true);
  wifiManager.setSaveConfigCallback(saveConfigCallback);
  wifiManager.autoConnect("AutoConnectAP");
  Serial.println("connected...yeey :)");

  status.SetPixelColor(0, blue);
  status.Show();

  if (shouldSaveConfig) {
    Serial.println("saving config");
    DynamicJsonDocument jsonBuffer(1024);

    File configFile = LittleFS.open("/config.json", "w");
    if (!configFile) {
      Serial.println("failed to open config file for writing");
    }

    serializeJsonPretty(jsonBuffer, Serial);
    serializeJson(jsonBuffer, configFile);
    configFile.close();
    //end save
    shouldSaveConfig = false;
  }

  webSocket.beginSSL("websocket.ericbetts.dev", 443);
  webSocket.setReconnectInterval(5000);
  webSocket.onEvent(webSocketEvent);
}


uint8_t breathe = 0;
bool forward = true;

void loop() {
  webSocket.loop();
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis > interval) {
    previousMillis = currentMillis;
    //keepalive
    webSocket.sendTXT("{}");
  }

/*
  if (forward) {
    breathe++;
  } else {
    breathe--;
  }

  if (breathe == 0) {
    forward = true;
  } else if (breathe == 128) {
    forward = false;
  }

  status.SetPixelColor(0, RgbColor(breathe));
  status.Show();
  delay(10);
*/
}
