import React, {useState, useRef} from 'react';

import {When} from 'react-if';

import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';

import {CirclePicker} from 'react-color';
//import tinycolor from 'tinycolor2';

import './App.css';

const numPixels = 24;

const initialPixel = {color: 'grey', selected: false}
const initialPixels = (new Array(numPixels)).fill(0, 0, numPixels).map((_, i) => {
  return {...initialPixel, index: i}
})

const colors = [
  "#f44336", "#e91e63", "#9c27b0", "#673ab7",
  "#3f51b5", "#2196f3", "#03a9f4", "#00bcd4",
  "#009688", "#4caf50", "#8bc34a", "#cddc39",
  "#ffeb3b", "#ffc107", "#ff9800", "#ff5722",
  "#795548", "#607d8b",
  "#000000", "#404040", "#808080", "#c0c0c0", "#ffffff"
]

function App() {
  const [lastStatus, setLastStatus] = useState('');
  const [selected, setSelected] = useState([]);
  const [pixels, setPixels] = useState(initialPixels)
  const svgElem = useRef(null);

  const colorSelect = async (color) => {
    if (selected.length === 0) {
      return;
    }
    const { rgb, hex } = color;
    const { r, g, b } = rgb

    const newPixels = [...pixels];
    selected.forEach(i => {
      newPixels[i].color = hex;
    });

    const body = {
      update: []
    }
    for (var i = 0; i < numPixels; i++) {
      if (selected.includes(i)) {
        if (r === g && g === b) {
          body.update.push({r: 0, g: 0, b: 0, w: r})
        } else {
          body.update.push({r, g, b, w: 0})
        }
      } else {
        body.update.push(null);
      }
    }

    try {
      const response = await fetch('/api', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(body)
      });
      const content = await response.text();
      if (response.ok) {
        setSelected([])
        setPixels(newPixels)
        setLastStatus(content);
        setTimeout(() => {
          setLastStatus('');
        }, 3000);
      }
    } catch (e) {
      console.log(e);
      this.setState({lastOk: false});
    }
  }

  function toggleAll() {
    if (selected.length === numPixels) {
      setSelected([]);
    } else {
      setSelected([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23]);
    }
  }

  const height = 500;
  const width = 500;
  const wedgeDegree = 360 / numPixels;
  const innerRadius = 150;
  const outerRadius = 200;

  function select(index) {
    if (selected.includes(index)) {
      setSelected(selected.filter(x => x !== index));
    } else {
      setSelected([...selected, index]);
    }
  }

  return (
    <>
      <Navbar collapseOnSelect expand="sm" bg="info" variant="info">
        <Navbar.Brand>Furious Dreaded Flannel</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="mr-auto">
          </Nav>
          <Nav>
            <Navbar.Text>
            </Navbar.Text>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
      <Container className="text-center justify-content-center align-items-center">
        <Row>
          <Col>
            <svg ref={svgElem} height={height} width={width}viewBox={`0 0 ${width} ${height}`} xmlns="http://www.w3.org/2000/svg">
              <g transform={`translate(${width/2},${height/2})`}>
                <g onClick={() => toggleAll()}>
                  <circle cx={0} cy={0} r={innerRadius} fill={selected.length === 24 ? "black" : "white"} stroke="black" />
                  <circle cx={0} cy={0} r={outerRadius} fillOpacity={0} stroke="black" />
                </g>

                {pixels.map((pixel, index) => {
                  const { color } = pixel;
                  return (
                    <g key={index} transform={`rotate(${index * wedgeDegree})`} onClick={() => select(index)}>
                      <line x1={innerRadius} y1={0} x2={outerRadius} y2={0} stroke={color} strokeWidth={40} strokeOpacity={selected.includes(index) ? 1 : .8  } />
                    </g>
                  );
                })}

              </g>
            </svg>
          </Col>
        </Row>
        <Row>
          <Col>
            <CirclePicker
              className="justify-content-center"
              width="100%"
              colors={colors}
              onChangeComplete={colorSelect}
            />
          </Col>
        </Row>
        <Row>
          <Col>
            <When condition={lastStatus.length > 0}>
              {lastStatus}
            </When>
          </Col>
        </Row>

      </Container>
      <footer className="d-none d-md-block page-footer font-small pt-5 mx-auto">
        <Container fluid className="text-center justify-content-center align-items-center">
          <Row>
            <Col>
            </Col>
          </Row>
        </Container>
      </footer>
    </>
  );
}

export default App;
